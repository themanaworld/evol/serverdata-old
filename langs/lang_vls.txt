Copyright (C) 2010-2011  Evol Online
*hips*
*hic*

... By the power vested in me,
... Deur de macht toegekend an ik,

Ah, she *hips* is here.
Ah, *hic* z'es hiere.

Are you tired of being what you are?
Zy 't nie moe vo jezelven te zin?

AreaNPC
AreaNPC

As you want!
Gelyk da je wilt!

Ash brown
Asbrun

Bald
Koale

Bee spawned
En bie es gespawned

Billy Bons
Billy Bons

Black
Zwarte

Blaf
Waf

Ble *hips*... Imma great sail*hips*or!
Bwe *hic*... Ik ben ne goei'n zeil*hic*er!

Blonde
Blonde

Blue
Blauw

Boots
Bott'n

Bowl cut
Gesne'en gelyk nen pot

Brown
Brunne

Brunette
Brunne

Bun
Dotjen

Bunny ears
Koninne oarn

Bye bye!
Salut!

Can you do something with my color?
Kun dje iet an myn kleur doene?

Cap
'n Klakke

Center parting/Short and slick
Center parting/Short and slick

Choppy
Gezoagd

Combed back
Noar achter'n gekamd

Continue?
Deurgoan?

Copper
Koper

Copper red
Roste

Could I ask you which language do you talk? Like that I add you on the list.
Maggekik ui vroagn wukke toale gy klapt? Zoda'k ui kanne toevoegn an de liste.

Demon
Duvel

Desert hat
Sahara 'oed

Devis
Devis

Did you bring me @@ @@?#0
E je vo mi @@ @@ gebracht?

Did you bring me @@ @@?#1
E je vo mi @@ @@ gebracht?

Do you know some interesting people around here?
Ken dje n poar giestige minsn hiere?

Do you want to create a bee?
Wil dje en by spawn'n?

Don't worry, I'm sure that everything will turn out well for the both of us!
Moak je gin zorgn, 'k ben zeker da't oal wel goe goat utdroain veur oes oalletwee!

Don't you see that I'm already talking?
Zie j' nie da'k oal an 't klappn ben?

Done.
Gereed.

Dyeing failed. You lost dye item.
't Kleur'n miste. Ge zit ui dinske kwit.

Dyer
Kleurder

Elmo
Elmo

Emo
Emo

Enora
Enora

Eurni
Eurni

Eurni the Surgeon
Eurni de Chirurg

Flat ponytail
Platt'n pony

For dye your item, you need white item and dye with required color.
Om te kleur'n, ei je 'n wit ding noadig en 'n kleurkstoffe in 't kleur da j' wilt.

Fushia
'n sorte poars

Ggrmm grmmm..
Ggrmm grmmm..

Gloves
Han'skoen'n

Good job!
Goe gedoan!

Green
Groene

Hahaha, I won't take much of your time, go ahead!
Hahaha, 'k goa nie vil tid va j' inneem'n, goa j' gang!

He takes a small list and starts reading it.
Ie pakt een kljen listje en begunt 't te lez'n.

Headband
'oofdband

Hehe. Me, I will try to open a market, I've heard that it is a big city of commerce here, because of the port and all...
Hehe. Ikke, 'k goa prober'n en kroam 't opnen, 'k e g'heurt da't ier en grot'n commèrce es hiere, deur d' hav'n ennezo...

Hello @@
'n Dag @@

Hello dear, what do you want today?#0
'allo, wuk wil dje vandoage?

Hello dear, what do you want today?#1
'allo, wuk wil dje vandoage?

Hello, girl!
'allo, meiske!

Hello, stranger!
'allo, vrjemden!

Here is your reward!
'ier es ui comisse!

Hey! Yes, I'm already awake. So, it's end of our little road together, what will you do you now?
'ey! Joa'k, 'k zin oal wakkre. Dus, 't es 't einde van oeze reis toope, wa wil dje nui goan doen?

Hey! You saw? we've arrived on the port today, it was a long way from the other island...#0
'ey! E j' 't gezien? we zin angekomn in den 'av'n vandoage, 't e verre van 't ander eiland...

Hey! You saw? we've arrived to the port today, it was a long way from the other island...#1
'ey! E j' 't gezien? we zin angekomn in den 'av'n vandoage, 't e verre van 't ander eiland...

Hey, girl!
'ey, meiske!

Hey, guy!
'ey, gast!

Hey, look at Julia, she's a beauty, isn't she? She's been looking at me for some minutes now... haha! Good luck, my friend.
'ey, kik noa Julia, es 't gin skwon meiske? Z' es oal efjes noar ik an 't koekeloern... haha! Veel geluk, moatje.

Hey, you lied to me.
'ey, g' e't gelogn tegen ikke.

Hmm, I'm fine for now, thank you.
Hmm, 'k zin ok nui, merci.

How can I get it
'oe kannekik da e'n

I can!
'k kan 't!

I don't need your help right now, come back later.
'k e uin hulpe nui nie nwodig, kom gie loater moar ne kje were.

I don't want change my language, sorry.
'k wille min toale nie verandern, sorry.

I gave you a reward. I can't give you more.
'k e j' oal en beloninge gegevn. 'kannekik ui niks mjer gevn.

I made a mistake, I would like to change my language.#0
'k e gemist, 'k zou geirn min toale verander'n.

I made a mistake, I would like to change my language.#1
'k e gemist, 'k zou geirn min toale verander'n.

I need help for cleaning the wedge of the ship, but you aren't strong enough to help me.
'k e 'n bikke hulpe nodig vo den onderkant van 't skip skwone te moakn, moa ge zy nie sterk genoeg vo mi 't helpn.

I need somebody who can clean the bottom of the ship, can you help me?
'k e 'n twiene nodig die 't 'n onderkant van 't skip kan skwone moakn, kun dje mi help'n?

I sentence you to death... twice.
'k verkloare gy vo dood... twee kje.

I speak Dutch
'k klappe Nederlands

I speak English
'k klappe Engels

I speak Flemish
'k klappe West-Vloams

I speak French
'k klappe Frans

I speak Greek


I speak Indonesian


I speak Italian
'k klappe Italioans

I speak Russian
'k klappe Russisch

I speak Spanish
'k klappe Spoans

I think that I will explore around a bit and see what I can find, and you?
'k peize da'k een bitje goa rondkikkn en zien wa da'k vind, en gy?

I think that the ship will still be here for some time, we need a lot of food and our ship needs some reparation before going back to the sea.
'k peize da't skip ier nog wel efkes goa stoan, w' e'n nen hoop eet'n nodig en oes skip moe nog vermakt word'n voordat 't weer in de zee kan goan.

I think you can help me, right?
'k peize da j' gy mi kunt helpn, juste?

I want to be a Human
'k wille nen Mens kom'n

I want to be a Ifitron
Ik wille nen Ifitron kom'n

I want to be a Orc
'k wille nen Orc kom'n

I want to be a Sparron
Ik wille nen Sparron kom'n

I want to be a Ukar
'k wille nen Ukar kom'n

I want to dye to different color
'k Wille kleur'n in en ander kleurke

I will give you @@gp.
'k goa ui @@gp gev'n.

I will need some coffee...
'k wille wa kaffie e'n...

I will rest a bit here or in the town, I don't feel so well after this trip...
'k goa iere of in 't dorp en bikke rustn. 'k voele mi nie zo goe achter deze reize...

I will!
'k goa 't doen!

I wonder about that, I think I'll discover bit by bit and see what happens!#0
'k vroage 't mi af, 'k peize da'k bitje bi bitje entwadde goa'n ontdekkn en zien wa da 't er verder gebeurt!

I wonder about that, I think I'll discover bit by bit and see what happens!#1
'k vroage 't mi af, 'k peize da'k bitje bi bitje entwadde goa'n ontdekkn en zien wa da 't er verder gebeurt!

I'd like to get a different style.
'k zou geirn nen andern coupe willn.

I'll come visit! I hope that this island will give us what we're looking for...
'k goe ui 'n visite doen! 'k hoope da 't eiland oes goa gevn woar wudder ip zoek noar zin...

I'll get it!#0
'k goa 't goan hoaln!

I'll get it!#1
'k goa 't goan hoaln!

I'm Julia, my job is to register every body which enter or exit the ship.
Ik benne Julia, minnen job es vo iedereen te registrern die van 't skip komt of goat.

I'm busy#0
'k zin bezig

I'm busy#1
'k zin bezig

I've a girlfriend, and she needs me to do something unavailable.
'k e en wuf, en ze vroagt mi iet onmeugelijks.

If you dont have it, then you cant dye.
Oa je 't nie eit, kundje gie nie kleur'n.

Imperial
Imperial

Infinitely long
Oneindig lank

It's *hips* wrong, Imma not *hips* drunken.
't es *hic* mis, Ikke zin nie *hic* dronk'n.

Julia
Julia

Khaki
Kaki

Kota
Kota

Leave alone my family treasure!
Loat min familie juweln me ruste!

Leave it
Loat 't liggn

Let me see your work...
Toon ne kje wa da'j' gedoan e't...

Let me sleep...
Loat mi sloapen...

Let me think...
Loat me ne kje peiz'n...

Lettuce
Saloa

Lime
'n Sorte groen

Lloyd
Lloyd

Long and curly
Lank en krull'nd

Long and slick
Lank en rechte

Long ponytail
Ne lange pony

Make less noise, please.
Moak ne kje wa minder lawaai, merci.

Mane
Moanen

Marko
Marko

Mauve
Mauve

Maybe you will be the next?#0
Misskien kom dje gy de volg'nden?

Maybe you will be the next?#1
Misskien kom dje gy de volg'nden?

Mini-skirt
Minirok

Mohawk
Mohawk

Move along, kid.
Loap moa deure, kienneke.

Navy blue
'n Sorte blauw

No
Nen

No problem, do you need something else?
Geen probleem, wil dje gy nog entwa d' anders doen?

No!
Nen!

No, thank you
Nen, merci

No, thanks!
Nen, merci!

No, they are way too dangerous for me!
Nen's, Ze zin vee te gevoarlik vo mi!

Not yet.
Nog nie..

Note
Nota

Nothing, sorry
Nietn, sorry

Now i have class
Nu e 'k ik klasse

Of course! Tell me which language you speak.
Moabajoa't! Zeg moa wukke toale da je klapt.

Off black
Bikans zwarte

Oh really? I don't have much time right now, I need to go. Sorry.
Oh es 't woar? 'k e nie veel tid nui, 'k moe goan. Sorry.

Oh, no problem! Anyway, if you have questions, you should see Julia at the reception, she's very nice!
Oh, geen probleem! In elk geval, oa je vroagen e't, kun dje 't vroag'n an Julia an de receptie, z' es vree vrindelik!

Oh, thank you! Yo, what would I do without your help. Ok, see you!
Oh, merci! Yo, wa zou 'k ik doen zonder gy. Ok, tot nog ne kje!

Ok, Done. You need something else?
Ok, Kloar. E je nog iets anders nodig?

Ok, ok. Come back if you change your mind.
Ok, ok. Kom moar ne kje were oa je van gedacht veranderd zit.

Ok, thanks
Ok, merci

Okay, I'm ready to work!#0
Oke, 'k zin kloar vo te werk'n!

Okay, I'm ready to work!#1
Oke, 'k zin kloar vo te werk'n!

Okay, you can start!
Oke, ge kunt start'n!

Once it's done, you will be kicked from the server. Don't panic, as everything is fine.
As 't kloar es, ga je gekicked word'n van de server. Ge moet nie paniker'n, 't es volkomn normoal.

Orange
Oranje

Pants
Broek

Parted
Gesplitst

Perky ponytail
Parmantigen pony

Peter
Peter

Pigtails
Vlechtjes

Pink
Roze

Please change my race
Verander min ras

Please do, my dear
Doe moa

Please select a race
Kies en ras

Pompadour
Pompadour

Punk
Punk

Purple
Poars

Rabbit ears
Koninne oar'n

Read it
Lees 't

Red
Rood

Robe
Kleed

Ronan
Ronan

Rrrr pchhhh...
Rrrr pchhhh...

Scarf
Sjoal

Server
Server

ServiceNPC
ServiceNPC

Shop 1
Shop 1

Shop 2
Shop 2

Shop 3
Shop 3

Shop 4
Shop 4

Shop 5
Shop 5

Shop 6
Shop 6

Shop 7
Shop 7

Short and curly
Kort en krull'nd

Short punk
Korte punk

Short robe
Kort kleed

Short tank top
Korte spaghettibandjes

Shorts
Short

Silver
Zilvre

Skirt
Rok

Sleeping Elf
Sloapende Elf

So die already!
Dus goa moa dood!

Sorcerer robe
Tovenoarskleed

Sorry you dont have components.
Sorry, g' e 't gin componentn.

Surprise me
Doe moa entwa

T-neck sweater
Dikke nekke boai

Tank top
Spaghettibandjes

Teal
Groenblauw

TestNPC
TestNPC

There is also @@ on the docks, it will be a pleasure for her to help you!
D'er es ook @@ an de dokkn, 't goa en plezierke zin veur hoar om ui t' help'n!

They are really nice and may help you much more than me.
Ze zin echt vriendelik en kunn'n ui misskien beter help'n dannekik.

This island is kinda funny for starting an adventure, and it's a good place for meeting people too, haha!
Dit eiland es nogal grappig vo 'n avontuur te startn, en 't es ook en goeie ploatse vo mensen t' ontmoet'n, haha!

This note was left by somebody.
Dezen nota was achtergelat'n deur entwiene.

Tousled layers
Warrige loagen

Turtles drop it. Good luck!
Skilpadden loat'n 't vall'n. Veel geluk!

V-neck sweater
V-kroage boai

Warin
Warin

Warning: All characters under this login will be changed.
Waarskuwinge: Oalle karakters by dezen login goan verander'n.

Wave
Zwoai

Wavy
Golv'nd

We are at [@@], it's a big port-city, many beginners start their adventure here.
Wudder zin in [@@], dat es en grote hoavenstad, veel beginners start'n udder avontuur 'iere.

Wedding
Trouw

Well, I'll see what happens. I hope that good luck will follow me around!
Wel, 'k goa wel zien wat dat er gebeurt. Ik hoope da 'k veel geluk goa en!

What can I dye for you today?
Wa kannekik kleur'n vo gy vandoage?

What color?
Wuk kleur?

What do you want to do with it?
Wa wil dje d'ermee goan doen?

What do you want to dye?
Wa wildje gie kleur'n?

What do you want today?
Wa wil dje vandoage doen?

What do you want?
Wa wil dje gie?

What will you do during this break?
Wa goe doen, nu da we'n pauze e'n?

What? Never!
Wa? Nooit!

What? This reward is too small!
Wa? Dezen beloninge es vee te kleine!

Where are we?
Woar zin we?

Where is this cup?..*hips*
Woar es dien beker?..*hic*

White
Wit

Why do you *hips* at me?!
Vowa doe je gie *hic* noar ikke?!

Why i cant dye my item?
Vowa kannekik min dinske niet kleur'n?

Why not, I need to train anyway.
Vowa nie, 'k moe toch nog en bikke train'n.

Wild
Wild

Witch hat
'eksen 'oed

Would you be interested in a race change?
Zou je gie ginteresseerd zin in en ras veranderinge?

Would you be interested in a sex change?
Zou je gie ginteresseerd zin in en geslachts veranderinge?

Yeah, but what reward will I get?
Mja, moa wukke beloninge krigge 'k ik?

Yeah, if you like, I'll open a market in this town, maybe you can visit me? It could be fun to see you again!
Mja, oa j' wilt, Goa 'k een kroam open'n in 't dorp, misskien kun dje mi ne kje kom'n en visite doen? 't zou vree geestig zin vo je were te ziene!

Yellow
Geel

Yes
Joa

Yes, I can!
Joa, da kannekik!

Yes, I did it!#0
Jah, 'k e 't gedoane!

Yes, I did it!#1
Jah, 'k e 't gedoane!

Yes, please!
Joa, giv moa!

Yo, I found it, cool! I have everything, except @@ @@. Can you bring it to me?
Yo, 'k e 't gevond'n, cool! 'k e 't oal, just nog @@ @@. Kun dje gie mi da breng'n?

Yo, can you help me, bro?
Yo, kun dje mi ne kje helpn, moat?

Yo, can you help me, sis?
Yo, kun dje mi ne kje helpn, meiske?

Yo, do you see what I mean?
Yo, zie je wa da'k bedoele?

Yo, look: my friend advised me to make a cool and tasty meal, and he gave me instructions.
Yo, kik: minne moat e mi gadviseerd vo iets cools en lekkers te moak'n vo 't eet'n, en ie e mi instructies gegevn.

Yo, now just bring it to me.
Yo, giv 't mi nui moa.

Yo, thank you!
Yo, merci!

Yo, that's good that you want to help me.
Yo, 't es goe da j' gy mi wilt help'n.

Yo, you really brought it!
Yo, g' e 't echt meegebroacht!

You can start now.
Ge kunt nui start'n.

You can't go there!
Ge mag doare nie goan!

You don't have enough to pay for my services.
G' e 't nie genoeg geld vo min dienst'n.

You failed the task!#0
G' e gefoald in ui toake!

You failed the task!#1
G' e gefoald in ui toake!

You should go see the sailors around here.#0
Ge zou de zeilers iere kunn'n ansprek'n.

You should go see the sailors around here.#1
Ge zou de zeilers iere kunn'n ansprek'n.

Zzzzzzzzzz
Zzzzzzzzzz

is helping me.
es mi an 't help'n.

