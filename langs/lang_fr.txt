Copyright (C) 2010-2011  Evol Online
*hips*
*hips*

... By the power vested in me,
... Par le pouvoir dont je suis investi,

Ah, she *hips* is here.
Ah, elle *hips* est là.

Are you tired of being what you are?
En as-tu marre d'être ce que tu es ?

AreaNPC
AreaNPC

As you want!
Comme tu veux... !

Ash brown
Marron cendré

Bald
Chauve

Bee spawned
Abeille est apparue

Billy Bons
Billy Bons

Black
Noir

Blaf
Blaf

Ble *hips*... Imma great sail*hips*or!
Blurp *hips*... Jeeeeu chuii un grand mar*hips*in !

Blonde
Blond

Blue
Bleu

Boots
Chaussures

Bowl cut
Coupe au bol

Brown
Marron

Brunette
Brun

Bun
Chignon

Bunny ears
Oreilles de Lapin

Bye bye!
Salut !

Can you do something with my color?
Pourriez-vous faire quelque chose pour ma couleur ?

Cap
Casquette

Center parting/Short and slick
Raie au milieu / Court et lisse

Choppy
Carré court sculpté

Combed back
En arrière Gominé

Continue?
Continuer ?

Copper
Cuivré

Copper red
Rouge cuivre

Could I ask you which language do you talk? Like that I add you on the list.
Puis-je te demander quelle langue tu parles ? De cette façon je peux t'ajouter dans la liste.

Demon
Démon

Desert hat
Keffieh

Devis
Devis

Did you bring me @@ @@?#0
M'avez-vous amené @@ @@ ?

Did you bring me @@ @@?#1
M'avez-vous amené @@ @@ ?

Do you know some interesting people around here?
Connais-tu des personnes intéressantes par ici ?

Do you want to create a bee?
Veux-tu faire apparaître une abeille ?

Don't worry, I'm sure that everything will turn out well for the both of us!
T'en fait pas, je suis certain que tout ira bien pour nous deux !!

Don't you see that I'm already talking?
Ne vois-tu pas que je suis déjà en conversation avec quelqu'un ?

Done.
Terminé.

Dyeing failed. You lost dye item.
La préparation de la couleur a échoué. Tu as perdu les objets nécessaires pour la faire.

Dyer
Teinturier

Elmo
Elmo

Emo
Emo

Enora
Enora

Eurni
Eurni

Eurni the Surgeon
Eurni le chirurgien

Flat ponytail
Queue de cheval plate

For dye your item, you need white item and dye with required color.
Pour teindre ton vêtement, tu as besoin de celui-ci en blanc et de la teinte de la couleur que tu veux employé.

Fushia
Fushia

Ggrmm grmmm..
Groumphh groumphh

Gloves
Gants

Good job!
Bon boulot !

Green
Vert

Hahaha, I won't take much of your time, go ahead!
Ahahahaha, je ne te prendrais pas plus de temps, continue !

He takes a small list and starts reading it.
Il prend une courte liste et commence à la lire.

Headband
Bandeau

Hehe. Me, I will try to open a market, I've heard that it is a big city of commerce here, because of the port and all...
héhé. Moi, je vais essayer d'ouvrir un magasin, j'ai entendu dire que c'est une grande ville d'échange et de commerce ici, à cause du port et tout ça...

Hello @@
Bonjour @@

Hello dear, what do you want today?#0
Coucou ma belle, que veux-tu aujourd'hui ?

Hello dear, what do you want today?#1
Salut mon beau, que veux-tu aujourd'hui ?

Hello, girl!
Bonjour mademoiselle !

Hello, stranger!
Salut étranger !

Here is your reward!
Voilà ta récompense !

Hey! Yes, I'm already awake. So, it's end of our little road together, what will you do you now?
Hé ! Oui ça va, je suis déjà au courant. Alors c'est la fin de notre périple ensemble... Que feras-tu maintenant ? 

Hey! You saw? we've arrived on the port today, it was a long way from the other island...#0
Hé ! tu vois nous sommes arrivés au port aujourd'hui, ce fut un long voyage depuis l'autre île...

Hey! You saw? we've arrived to the port today, it was a long way from the other island...#1
Hé ! tu vois nous sommes arrivés au port aujourd'hui, ce fut un long voyage depuis l'autre île...

Hey, girl!
Hé, Mademoiselle !

Hey, guy!
Hé, Jeune homme !

Hey, look at Julia, she's a beauty, isn't she? She's been looking at me for some minutes now... haha! Good luck, my friend.
Hé, regarde Julia... elle est belle, n'est-ce pas ? Elle ne cesse de me regarder depuis quellesque minutes... Ahaha bonne chance mon ami.

Hey, you lied to me.
Hé toi, tu m'as menti.

Hmm, I'm fine for now, thank you.
Hum, je vais bien pour l'instant, merci.

How can I get it
Comment puis-je l'obtenir ?

I can!
Je le peux !

I don't need your help right now, come back later.
Je  n'ai pas besoin de ton aide pour l'instant. Reviens plus tard.

I don't want change my language, sorry.
Je ne veux pas changer de langue, désolé

I gave you a reward. I can't give you more.
Je t'ai déjà donné une récompense. Je ne peux t'en donner une autre.

I made a mistake, I would like to change my language.#0
J'ai fait une erreur, je souhaiterais changer ma langue.

I made a mistake, I would like to change my language.#1
J'ai fait une erreur, je souhaiterais changer de langue.

I need help for cleaning the wedge of the ship, but you aren't strong enough to help me.
J'ai besoin d'aide pour nettoyer le pont du bateau, mais tu n'es pas assez costaud pour m'aider.

I need somebody who can clean the bottom of the ship, can you help me?
J'ai besoin d'aide pour nettoyer le fond de ce bateau, peux-tu m'aider ?

I sentence you to death... twice.
Je te condamne à mort... par deux fois.

I speak Dutch
Je parle hollandais

I speak English
Je parle anglais

I speak Flemish
Je parle flamand

I speak French
Je parle français

I speak Greek


I speak Indonesian


I speak Italian
Je parle italien

I speak Russian
Je parle russe

I speak Spanish
Je parle espagnol

I think that I will explore around a bit and see what I can find, and you?
Je crois que je vais allez explorer un peu par ici et voir ce que je peux trouver, et toi ?

I think that the ship will still be here for some time, we need a lot of food and our ship needs some reparation before going back to the sea.
Je pense que le bateau va rester là encore un moment, nous avons besoin de beaucoup de provisions et notre bateau a besoin de réparations avant de pourvoir reprendre la mer.

I think you can help me, right?
Je pense que tu peux m'aider, je me trompe ?

I want to be a Human
Je veux être un Humain

I want to be a Ifitron
Je veux être un Ifitron

I want to be a Orc
Je veux être un Orque

I want to be a Sparron
Je veux être un Sparron

I want to be a Ukar
Je veux être un Ukar

I want to dye to different color
Je voudrais le teindre d'une autre couleur

I will give you @@gp.
Je te donnerais @@gp.

I will need some coffee...
Je vais avoir besoin de quelsques cafés...

I will rest a bit here or in the town, I don't feel so well after this trip...
Je vais me reposer un peu ici ou en ville, je ne me sens pas très bien après ce voyage...

I will!
Je le ferais !

I wonder about that, I think I'll discover bit by bit and see what happens!#0
Je me le demande. Je crois que je vais découvrir petit à petit et voir comment les choses se déroulent !

I wonder about that, I think I'll discover bit by bit and see what happens!#1
Je me le demande... Je crois que je vais découvrir petit à petit et voir comment les choses se déroulent !

I'd like to get a different style.
J'aimerais changer de style.

I'll come visit! I hope that this island will give us what we're looking for...
Je viendrais vous rendre visite ! J'espère que cette île nous apportera ce que nous recherchons...

I'll get it!#0
Je l'aurais !

I'll get it!#1
Je l'aurais !

I'm Julia, my job is to register every body which enter or exit the ship.
Je suis Julia. Mon travail est de faire la liste de tous ceux qui entrent ou sortent de ce navire.

I'm busy#0
Je suis occupée !

I'm busy#1
Je suis occupé !

I've a girlfriend, and she needs me to do something unavailable.
J'ai une petite amie, et elle a besoin que je fasse pour elle quelque chose d'impossible. 

If you dont have it, then you cant dye.
Si tu n'en as pas, tu ne peux pas le teindre.

Imperial
Imperiale

Infinitely long
Infiniment long

It's *hips* wrong, Imma not *hips* drunken.
C'est pas *hips* vrai, je ne suis pas *hips* bourré.

Julia
Julia

Khaki
Khaki

Kota
Kota

Leave alone my family treasure!
Ne touchez pas à mes bijoux de famille !

Leave it
Laisse ça

Let me see your work...
Laisse moi jeter un oeil à ton travail...

Let me sleep...
Laisse moi dormir...

Let me think...
Attends que j'y pense...

Lettuce
Laitue

Lime
Citron

Lloyd
Lloyd

Long and curly
Long et bouclé

Long and slick
Long et lisse

Long ponytail
Longue queue de cheval

Make less noise, please.
Fais moi de bruit s'il te plait.

Mane
Pique

Marko
Marko

Mauve
Mauve

Maybe you will be the next?#0
Peut être seras-tu le prochain ?

Maybe you will be the next?#1
Peut être seras-tu le prochain ?

Mini-skirt
Mini jupe

Mohawk
Raie au milieu

Move along, kid.
Ce n'est pas une place pour toi, file gamin.

Navy blue
Bleu marine

No
Non

No problem, do you need something else?
Pas de problème, tu as besoin de quelque chose d'autre ?

No!
Non !

No, thank you
Non, merci

No, thanks!
Non. Merci !

No, they are way too dangerous for me!
Non, ils sont bien trop dangereux pour moi !

Not yet.
Pas encore.

Note
Note

Nothing, sorry
Rien, désolé

Now i have class
Je suis maintenant la classe, le top

Of course! Tell me which language you speak.
Bien sur, dis moi quelle langue tu parles.

Off black
Noir profond

Oh really? I don't have much time right now, I need to go. Sorry.
Oh vraiment ? Je n'ai pas trop de temps maintenant, je dois y aller. Désolé.

Oh, no problem! Anyway, if you have questions, you should see Julia at the reception, she's very nice!
Oh pas de problème ! De toute façon, si tu as des questions, tu devrais aller voir Julia à la réception, elle est charmante !

Oh, thank you! Yo, what would I do without your help. Ok, see you!
Oh merci ! Que ferais-je sans ton aide. A bientôt !

Ok, Done. You need something else?
Ca y est c'est fait. tu as besoin de quelque chose d'autre ?

Ok, ok. Come back if you change your mind.
Ok Ok. Reviens si tu changes d'avis.

Ok, thanks
D'accord, merci

Okay, I'm ready to work!#0
OK je suis prête à me mettre au travail !

Okay, I'm ready to work!#1
OK je suis prêt à me mettre au travail !

Okay, you can start!
ca y est, vous pouvez commencer !

Once it's done, you will be kicked from the server. Don't panic, as everything is fine.
Une fois cela fini, tu seras expulsé du serveur. Ne panique pas, tout est normal.

Orange
Orange

Pants
Pantalon

Parted
Gominé

Perky ponytail
Queue de cheval chic

Peter
Peter

Pigtails
Couettes

Pink
Rose

Please change my race
S'il vous plait, changer la nature de mon personnage

Please do, my dear
S'il vous plait, allez-y mon cher...

Please select a race
Veuillez choisir la nature de votre personnage

Pompadour
Pompadour

Punk
Punk

Purple
Violet

Rabbit ears
Oreille de lièvre

Read it
Lis cela

Red
Rouge

Robe
Robe

Ronan
Ronana

Rrrr pchhhh...
Rrrr pchhhh...

Scarf
Echarpe

Server
Serveur

ServiceNPC
ServiceNPC

Shop 1
Magasin 1

Shop 2
Magasin 2

Shop 3
Magasin 3

Shop 4
Magasin 4

Shop 5
Magasin 5

Shop 6
Magasin 6

Shop 7
Magasin 7

Short and curly
Court et bouclé

Short punk
Punk court

Short robe
Robe courte

Short tank top
T shirt court

Shorts
Short

Silver
Argent

Skirt
Jupe

Sleeping Elf
Elfe dormeur

So die already!
Alors déjà mort !

Sorcerer robe
Robe de sorcier

Sorry you dont have components.
Désolé, vous n'avez pas assez d'ingrédients.

Surprise me
Surprends moi

T-neck sweater
Pull à col roulé

Tank top
T shirt

Teal
Aigue Marine

TestNPC
TestNPC

There is also @@ on the docks, it will be a pleasure for her to help you!
Il y a beaucoup de @@ sur les quais, ce sera un plaisir pour elle de t'aider !

They are really nice and may help you much more than me.
Ils sont vraiment gentils et ils peuvent t'aider bien plus que je ne le puis.

This island is kinda funny for starting an adventure, and it's a good place for meeting people too, haha!
Cette île a des côtés sympa pour commencer l'aventure, et c'est un bon endroit pour y rencontrer du monde aussi, ahah !

This note was left by somebody.
Cette note a été laissé par quelqu'un.

Tousled layers
Ébouriffés

Turtles drop it. Good luck!
Les tortues les jettent. Bonne chance !

V-neck sweater
Pul à col en V

Warin
Warin

Warning: All characters under this login will be changed.
Attention : Tous les personnages se trouvant sous cet identifiant seront changés.

Wave
Vague

Wavy
Ondulé

We are at [@@], it's a big port-city, many beginners start their adventure here.
Nous sommes à [@@], c'est une grande ville portuaire, nombre de personnes y commencent leurs aventures.

Wedding
Marriage

Well, I'll see what happens. I hope that good luck will follow me around!
Bien, je verrais ce qui se passera. J''espère que ma bonne étoile m'accompagnera !

What can I dye for you today?
Que puis-je teindre pour toi aujourd'hui ?

What color?
Quelle couleur ?

What do you want to do with it?
Que veux-tu faire avec cela ?

What do you want to dye?
Que veux-tu teindre ?

What do you want today?
Que veux-tu faire aujourd'hui ?

What do you want?
Que veux-tu ?

What will you do during this break?
Que feras-tu pendant cette pause ?

What? Never!
Quoi ? Jamais !

What? This reward is too small!
Quoi ? !!! Cette récompense est si infime !

Where are we?
Où sommes-nous ?

Where is this cup?..*hips*
Où est mon verre ?... *hips*

White
Blanc

Why do you *hips* at me?!
Pourquoi tu me *hips* regardes comme ça *hips* ?

Why i cant dye my item?
Pourquoi ne puis-je pas teindre mes vêtements ?

Why not, I need to train anyway.
Pourquoi pas ? j'ai besoin de m'entrainer de toute façon.

Wild
Sauvage

Witch hat
Chapeau de sorcière

Would you be interested in a race change?
Souhaiterais-tu changer la nature de ton personnage ?

Would you be interested in a sex change?
Souhaiterais-tu changer de sexe ?

Yeah, but what reward will I get?
Oui, mais comment serais-je récompensé ?

Yeah, if you like, I'll open a market in this town, maybe you can visit me? It could be fun to see you again!
Oui, si tu veux. J'ouvrirais un commerce dans cette ville, tu pourrais m'y rendre visite ? Ce serait sympa de te revoir !

Yellow
Jaune

Yes
Oui

Yes, I can!
Oui, je peux !

Yes, I did it!#0
Oui, j'ai réussi !

Yes, I did it!#1
Oui, j'ai réussi !

Yes, please!
Oui s'il te plait !

Yo, I found it, cool! I have everything, except @@ @@. Can you bring it to me?
Oui, je l'ai trouvé, cool ! J'ai tout sauf ça : @@ @@. Tu peux m'en ramener ?

Yo, can you help me, bro?
Salut, tu peux m'aider mon frère ?

Yo, can you help me, sis?
Salut, tu peux m'aider frangine ?

Yo, do you see what I mean?
Salut, tu vois ce que je veux dire ?

Yo, look: my friend advised me to make a cool and tasty meal, and he gave me instructions.
Salut, regarde, mon amie m'a conseillé de préparer un bon petit repas sympa et m'a donné des instructions.

Yo, now just bring it to me.
Salut, Maintenant tu as juste à me les ramener.

Yo, thank you!
Salut et merci !

Yo, that's good that you want to help me.
Salut, c'est sympa que tu veuilles bien m'aider.

Yo, you really brought it!
Salut, tu l'as vraiment ramené !

You can start now.
Tu peux commencer maintenant.

You can't go there!
Tu ne peux pas y aller !

You don't have enough to pay for my services.
Vous n'avez pas assez pour payer mes services.

You failed the task!#0
Tu as failli dans l'exécution de ta tâche !

You failed the task!#1
Tu as raté ta mission !

You should go see the sailors around here.#0
Tu devrais aller voir les marins dans le coin.

You should go see the sailors around here.#1
Tu devrais aller voir les marins dans le coin.

Zzzzzzzzzz
Zzzzzzzzzz

is helping me.
m'aide.

